import React from 'react';
import {View, ActivityIndicator } from 'react-native';


const Loader = () => {

    return (
        <View style={{
            flex:1,
            justifyContent: "center",
            alignItems:"center",
            backgroundColor: 'rgba(52, 52, 52, 0.05)',
           
        }}>

            <ActivityIndicator size="large" color="#000000" />

        </View>
    );
}

export default Loader 