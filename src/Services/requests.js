
import { getHelper } from "./RequestHelper";


export const getMovies = () => {

    return getHelper("https://yts.mx/api/v2/list_movies.json", null)
}
