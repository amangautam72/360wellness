export default ScreenName = {
    SPLASH: "SPLASH",
    LOGIN : "LOGIN",
    ONBOARD : "ONBOARD",
    MOVIE_LIST: "MOVIE_LIST"
}