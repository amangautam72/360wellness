import React,{useEffect} from 'react';

import { createStackNavigator } from '@react-navigation/stack'
import LoginScreen from '../Screens/Login';
import ScreenNames from '../Constants/ScreenNames';
import OnboardScreen from '../Screens/Onboard';
import MovieListScreen from '../Screens/MovieList';
import SplashScreen from '../Screens/Splash';

const Stack = createStackNavigator()

export default function MainStack(){


    // handleNotification()

    return (
        <Stack.Navigator>
             <Stack.Screen options={{headerShown:false}} name={ScreenNames.SPLASH} component={SplashScreen}></Stack.Screen>
            <Stack.Screen options={{headerShown:false}} name={ScreenNames.ONBOARD} component={OnboardScreen}></Stack.Screen>
            <Stack.Screen options={{headerShown:false}} name={ScreenNames.LOGIN} component={LoginScreen}></Stack.Screen>
            <Stack.Screen options={{headerShown:false}} name={ScreenNames.MOVIE_LIST} component={MovieListScreen}></Stack.Screen>
           
            
        </Stack.Navigator>
    )
}