import React from 'react';
import {SafeAreaView, Text, TextInput, View, Image, TouchableOpacity} from 'react-native';
import styles from './index.css';

import logo from '../../asset/logo.png';
import ScreenNames from '../../Constants/ScreenNames';

const OnboardScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>

      <Image source={logo} />

      <Text style={{ textAlign:"center", fontWeight:"bold", fontSize:20, marginVertical:100 }}>WELCOME</Text>

      <TouchableOpacity
      onPress={() => navigation.navigate(ScreenNames.LOGIN)}
       style={styles.button}>
          <Text style={{color:"white", textAlign:"center", fontWeight:"bold"}}>Login</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default OnboardScreen;
