import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import styles from './index.css';

import star from '../../asset/star.png';
import {getMovies} from '../../Services/requests';
import Loader from '../../UtilityComponents/Loader';

const MovieListScreen = () => {
  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getMovies()
      .then(response => {
        console.log(response);
        if (response.status == 200) {
          if (response.data.status == 'ok') {
            setList(response.data.data.movies);
            setIsLoading(false);
          }
        }
      })
      .catch(err => setIsLoading(false));
  }, []);

  const renderItem = ({item}) => (
    <View style={styles.item}>
      <Image
        source={{uri: item.medium_cover_image}}
        style={{height: 100, width: 100}}
      />

      <View style={styles.itemSection}>
        <Text style={{fontSize: 18}}>{item.title_long}</Text>

        <View style={{flexDirection: 'row'}}>
          {/* {[...Array(item.rating).keys()].map((element, index) => (
            <Image source={star}/>
          ))} */}
          {Array.from(Array(10).keys()).map(element => (
            <Image
              style={{width: 15, resizeMode: 'contain', margin: 2}}
              source={star}
            />
          ))}
        </View>
        <View style={{flexDirection: 'row'}}>
          {item.genres.map((element, index) => (
            <Text style={{color: 'darkblue'}}>{`${element}${
              index < item.genres.length - 1 ? ' | ' : ''
            }`}</Text>
          ))}
        </View>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.headerText}>Movie Turbo</Text>
      {isLoading ? (
        <Loader />
      ) : (
        <FlatList
          data={list}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      )}
    </SafeAreaView>
  );
};

export default MovieListScreen;
