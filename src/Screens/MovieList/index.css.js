import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
  item: {
    flexDirection: 'row',
    padding: 10,
    margin: 10,
    borderRadius: 5,
    backgroundColor: 'white',
    shadowColor: 'lightgrey',
    shadowOffset: {width: 4, height: 4},
    shadowOpacity: 0.8,
    shadowRadius: 1,
    alignItems: 'center',
  },
  itemSection: {
    flex: 1,
    paddingHorizontal: 10,
    // justifyContent:"space-between"
  },
  button: {
    padding: 12,
    backgroundColor: 'lightblue',
    borderRadius: 20,
    alignSelf: 'stretch',
    margin: 20,
    marginHorizontal: 50,
  },
  highlight: {
    fontWeight: '700',
  },
});

export default styles;
