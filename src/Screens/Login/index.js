import React from 'react';
import {SafeAreaView, Text, TextInput, View, Image, TouchableOpacity} from 'react-native';
import styles from './index.css';

import logo from '../../asset/logo.png';
import ScreenNames from '../../Constants/ScreenNames';

const LoginScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <Image source={logo} />

      <View style={{alignSelf:"stretch", marginTop:20}}>
        <TextInput
          style={styles.input}
          placeholder="Username"
          // onChangeText={onChangeText}
          // value={text}
        />
        <TextInput
          style={styles.input}
          // onChangeText={onChangeNumber}
          // value={number}
          placeholder="Password"
          keyboardType="numeric"
        />
      </View>

      <TouchableOpacity 
      onPress={() => navigation.navigate(ScreenNames.MOVIE_LIST)}
      style={styles.button}
      >
          <Text style={{color:"white", textAlign:"center"}}>Login</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default LoginScreen;
