import React, { useEffect } from 'react';
import {SafeAreaView, Text, TextInput, View, Image, TouchableOpacity} from 'react-native';
import styles from './index.css';

import logo from '../../asset/logo.png';
import ScreenNames from '../../Constants/ScreenNames';

const SplashScreen = ({navigation}) => {

  useEffect(() => {
    setTimeout(() => {
      navigation.navigate(ScreenNames.ONBOARD)
    },3000)
  },[])

  return (
    <SafeAreaView style={styles.container}>

      <Image source={logo} />

    </SafeAreaView>
  );
};

export default SplashScreen;
