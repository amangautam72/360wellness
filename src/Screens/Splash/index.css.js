import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:"center",
    backgroundColor:"white",
    alignItems:"center",
  },
  input: {
    padding:12,
    borderRadius:20,
    borderWidth:1,
    borderColor: "lightgrey",
    // alignSelf:"stretch",
    marginHorizontal:40,
    margin:10
  },
  button: {
    padding:12,
    backgroundColor: "lightblue",
    borderRadius:20,
    alignSelf:"stretch",
    margin:20,
    marginHorizontal:50
  },
  highlight: {
    fontWeight: '700',
  },
});

export default styles;
